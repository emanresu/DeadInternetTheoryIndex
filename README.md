# 🪦 Dead Internet Theory Index ☠️

### A list of chat rooms and chans and forums that aren't controlled by big tech and allow free speech. 

This index will exclude most projects based on crypto currency as i beleive most of them to be outright scams and the ones that seem to have legit use cases could still one day be usurped by a better newer coin thus jeopardizing whatever is built on top of them.

Contribute to this list by making a pull request.

### What is the dead internet theory?
It's the belief that the internet has been taken over by big corporate interests and dumbed down with the goal of creating digital consumers. This video: https://youtu.be/BEIZHlfjAT8 explains it best.

### Thoughts on forced HTTPS/SSL
Basically the way i see it is big tech has covertly implemented a killswitch. There currently isn't an alternative to DNS so users are forced to use it to access sites on the web. So domain names have from the start, been a piece of digital real estate. the problem i have with this is just like with home ownership or "owning" really anything material, is there is a central authority that keeps track of who owns what and that authority if pressured by special interests or governments can and will take that property away from you. and in the case of SSL certs, all the major browser companies have since around 2014ish, FORCED ssl onto the end user. sure the user is presented with a scary screen that warns them (or rather scared them) of some impending doom should they continue on, such as being hacked or a hacker seeing something etc. and they've even gone as far as obfuscating the "bypass" button in those warning screens, so now the average user has essentially been blocked from visiting a plain text http site. one might argue that you can still continue and look at them if you want, sure but only at great inconvenience. so most people simply don't. And as for the SSL cert authority itself, it is also subject to the tests of social and government pressures. if they are the issuer of a cert for a website that someone or some government doesnt like, then all that needs to be done is threaten that authority with fines or other penalties until they "play ball" and revoke the cert. That is essentially a low key killswitch in a nutshell.

So this combined with everyone hosting everything on big tech owned VPS farms is a big part of why the internet is dying. Everyone is still under the impression that the internet is some huge wide open unexplored wilderness, but in reality it's just a heavily locked down, monitored, comodified, centralized service where everyone at home as essentially a dumb terminal that connects to just a handful of big tech VPS infrastructure.

To solve all this we need a way around DNS. a way to map sites to their location without having to rely on ip addresses (which often change) and not rely on central authority to tell us what is located where. and secondly we need to start hosting our sites from home. but that can be a challenge. and possibly open the user to liablities and other assorted risks.

### Project ideas from our members 🧠
+ Decentralized chat program based on KAD with store and forward capabilites.
+ Creation of a search engine or multiple search engines.
+ Creation of a decentralized chat software that utilizes bittorrents DHT
+ Creation of a decentralized DNS software that isn't built on top of the crypto-currency meme
+ Bridge the IRC channel and Matrix room and XMPP

### Non-big tech controlled chat rooms 💬
+ [Official dead internet theory matrix room](https://matrix.to/#/#deadinternettheory:matrix.org)
+ Official dead internet theory IRC channel: irc.rizon.net #deadinternettheory
    + [Web chat](https://kiwiirc.com/nextclient/irc.rizon.net)

### Image boards
+ [AllChans](https://allchans.org/)
+ [Chan List](https://rentry.co/chans)

### Forums
+ Kiwii Farms
    + [Tor Link](http://kiwifarmsaaf4t2h7gc3dfc5ojhmqruw2nit3uejrpiagrxeuxiyxcyd.onion)
    + [Clearnet Link](http://kiwifarmsaaf4t2h7gc3dfc5ojhmqruw2nit3uejrpiagrxeuxiyxcyd.onion.ws)

### Non-big tech controlled search engines 🔍
+ [YaCy](https://yacy.net/)

### Alternative video hosting sites 🎥
+ [BitChute](https://www.bitchute.com/)
+ [Odysee](https://odysee.com/)

### Personal media server software 🍿
+ [Jellyfin](https://jellyfin.org/downloads/)

### Image/text board software
+ [overscript.net](https://overscript.net/)

### Chat software (Non-Vetted)
This software may or may not respect your privacy, it's listed here only as a resource.
+ [RetroShare](https://github.com/RetroShare/RetroShare)
+ [Richochet Refresh](https://www.ricochetrefresh.net/)
+ [Briar](https://briarproject.org/)
+ Matrix
    + [List of clients](https://matrix.org/clients/)
+ XMPP
    + [List of XMPP clients](https://xmpp.org/software/clients/)
    + [List of XMPP servers](https://list.jabber.at/)
+ Tox
    + [qTox](https://github.com/qTox/qTox/blob/master/README.md#qtox)
    + [Toxygen](https://github.com/toxygen-project/toxygen)
    + [Toxic](https://github.com/Jfreegman/toxic)
    + [aTox](https://github.com/evilcorpltd/aTox)

### Decentralized file sharing software 🖧
+ [Tribler](https://www.tribler.org/)
+ [Shareaza](http://shareaza.sourceforge.net/)
+ [RetroShare](https://github.com/RetroShare/RetroShare)
+ [WebTorrent](https://github.com/webtorrent/webtorrent)
+ [aMule](https://github.com/amule-project/amule)
+ [eMule](https://www.emule-project.com/)
+ [PirateBox](https://en.wikipedia.org/wiki/PirateBox)
> On 17 November 2019, Matthias Strubel announced the closure of the Pirate Box project, citing more routers having locked firmware and <u><b>browsers forcing https.</b></u>

### Decentralized networks 🌐
Networks such as zeronet and CJDNS have been omitted intentionally due to being crypto based. Sorry not sorry.
+ [Urbit](https://urbit.org/)
+ [i2p](https://geti2p.net/)
+ [IPFS](https://ipfs.tech/)
+ [GNUnet](https://www.gnunet.org/en/index.html)
+ Tor
    + [Tor Browser Bundle](https://www.torproject.org/download/)
    + [Tor Daemon](https://community.torproject.org/onion-services/setup/install/)

### Decentralized social media
+ [MinesTRIX](https://gitlab.com/minestrix/minestrix-flutter)
+ [Movim](https://github.com/movim/movim)

### Decentralized metaverse
+ [third room](https://thirdroom.io/preview)

### Alternative Operating Systems 💿
+ [Serenity OS](https://serenityos.org/)
+ [ReactOS](https://reactos.org)
+ [OpenBSD](https://www.openbsd.org/)

### Indexes from other members 🤝
+ [Peggle3's Index](https://codeberg.org/peggle3/ThePeggleIndex/src/branch/peggleindex)

### Tor indexes 🧅
+ [Tor taxi](http://tortaxi7axhn2fv4j475a6blv7vwjtpieokolfnojwvkhsnj7sgctkqd.onion/)
+ [darknet book](https://github.com/darknet-book/tor-guide)

### Dead sites and software RIP ⚰️
> Remember what they took from you.
+ [Dissenter](https://en.wikipedia.org/wiki/Gab_(social_network)#Dissenter)
+ liveleak.com
+ 8chan
+ 8kun
+ yahoo answers
+ [crypto.cat](https://en.wikipedia.org/wiki/Cryptocat)
+ voat.co
+ [torchat](https://en.wikipedia.org/wiki/TorChat)